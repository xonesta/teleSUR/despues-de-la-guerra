FROM node:10 as build

# Copy NPM manifests and install dependencies
WORKDIR /app
COPY yarn.lock package.json ./
RUN yarn

# Copy source and build
COPY src src
COPY public public
RUN yarn build --production

# Copy the rest files
COPY . .

# Second stage (run)
FROM nginx:stable-alpine

ENV SERVER_NAME _
ENV SERVER_PORT 5000

# Copy build from previous stage
COPY --from=build /app/build /www

# Copy any nginx config
COPY site.conf.template .

CMD DOLLAR=$ envsubst < site.conf.template > /etc/nginx/conf.d/default.conf && echo "Starting nginx on port ${SERVER_PORT}" && exec nginx -g 'daemon off;'
