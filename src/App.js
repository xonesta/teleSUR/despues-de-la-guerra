import React, { Component } from "react";
import Slides from "./components/Slides";
import Gallery from "./components/Gallery";
import { BrowserRouter as Router, Route } from "react-router-dom";
import countries from "./data/countries";
import es from "./data/es";
import en from "./data/en";
import "./assets/sass/main.scss";

class App extends Component {
  constructor() {
    super();
    this.state = {
      lang: "es"
    };
  }

  changeLang(lang) {
    this.setState({ lang });
  }

  render() {
    const messages = { en, es }[this.state.lang];
    console.log(this.state.lang);
    return (
      <Router>
        <div>
          <div className="page-loader" id="page-loader">
            <div>
              <div className="icon ion-spin" />
              <p>Cargando</p>
            </div>
          </div>

          <Route
            path="/"
            exact
            render={props => (
              <Slides
                {...props}
                lang={this.state.lang}
                countries={countries}
                messages={messages}
                changeLang={this.changeLang.bind(this)}
              />
            )}
          />
          <Route
            path="/:country/gallery"
            render={({ match }) => (
              <Gallery
                country={countries.find(
                  country => country.id === match.params.country
                )}
                lang={this.state.lang}
              />
            )}
          />
        </div>
      </Router>
    );
  }
}

export default App;
