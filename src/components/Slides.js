import React, { Component } from "react";
import Header from "./Header";
import HomeSlide from "./HomeSlide";
import CountrySlide from "./CountrySlide";

class Slides extends Component {
  render() {
    const { countries, messages } = this.props;

    return (
      <div>
        <Header {...this.props} />

        <div className="page-cover" id="parallax-cover">
          <div
            data-depth="0.2"
            className="cover-bg bg-img"
            data-image-src="img/portada.jpg"
          />
          <div
            className="cover-bg-mask bg-color"
            data-bgcolor="rgba(2, 3, 10, 0.7)"
          />
        </div>

        <main className="page-main page-fullpage main-anim" id="mainpage">
          <HomeSlide messages={messages} />
          {countries.map(country => (
            <CountrySlide key={country.id} country={country} {...this.props} />
          ))}
        </main>

        <footer id="site-footer" className="page-footer">
          <div className="footer-left">
            <ul className="social">
              <li>
                <a href="https://www.youtube.com/user/telesurtv">
                  <i className="icon fa fa-youtube" />
                </a>
              </li>
              <li>
                <a href="https://www.facebook.com/teleSUR">
                  <i className="icon fa fa-facebook" />
                </a>
              </li>
              <li>
                <a href="https://twitter.com/teleSURtv">
                  <i className="icon fa fa-twitter" />
                </a>
              </li>
            </ul>
          </div>
        </footer>
      </div>
    );
  }
}

export default Slides;
