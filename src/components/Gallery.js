import React, { Component } from "react";

class Gallery extends Component {
  render() {
    const { country } = this.props;
    return (
      <div>
        <header className="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right">
          <a href={`/#${country.id}`} className="x" />
        </header>
        <main className="page-main page-fullpage main-anim" id="gallerypage">
          {country.gallery.map(item => (
            <GalleryItem src={item} key={item} />
          ))}
        </main>
      </div>
    );
  }
}

class GalleryItem extends Component {
  render() {
    return (
      <div
        className="section section-gallery-item fp-auto-height-responsive main-home"
        data-section={`${this.props.src}`}
      >
        <div
          className="section-cover-full fit mask-layer bg-img"
          data-image-src={`/img/${this.props.src}`}
        />
        <div className="section-wrapper fullwidth with-margin pt-0">
          <div className="section-aside aside-bottom anim gallery-nav">
            <div className="btns-action anim-4 btns-transp-arrow-margin">
              <a
                className="btn btn-transp-arrow btn-primary icon-only scroll up"
                href="#"
              >
                <span className="icon arrow-left" />
              </a>
              <a
                className="btn btn-transp-arrow btn-primary icon-only scroll down"
                href="#"
              >
                <span className="icon arrow-right" />
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Gallery;
