import React, { Component } from "react";

class CountrySlide extends Component {
  render() {
    const { country, lang, messages } = this.props;

    return (
      <div
        className="section section-description fp-auto-height-responsive fullscreen-md backgrounded"
        style={{ backgroundImage: `url(img/${country.background})` }}
        data-section={country.id}
      >
        <div className="section-wrapper fullwidth with-margin">
          <div
            className="cover-bg-mask bg-color transformed"
            data-bgcolor="rgba(2, 3, 10, 0.7)"
          />
          <div className="section-content anim">
            <div className="row">
              <div className="col-12 col-sm-5 text-center">
                <div className="title-desc anim-2">
                  <img
                    alt={country.name[lang]}
                    src={`img/${country.id}_heading_${lang}.png`}
                    className="img-fluid"
                  />
                </div>
                {country.video && (
                  <div className="anim-3">
                    <a href={`#video-${country.id}`} rel="modal:open">
                      <img
                        alt="video thumbnail"
                        className="img-fluid video-thumbnail"
                        src={`img/${country.id}_thumbnail.jpg`}
                      />
                    </a>
                    <div
                      className="vcms-wrapper modal"
                      style={{ paddingBottom: "75.0%" }}
                      id={`video-${country.id}`}
                    >
                      <iframe
                        src={country.video[lang]}
                        scrolling="no"
                        frameBorder="0"
                        webkitallowfullscreen="true"
                        mozallowfullscreen="true"
                        allowFullScreen={true}
                        title={`Video: ${country.name[lang]}`}
                        style={{
                          position: "absolute",
                          width: "100%",
                          height: "100%",
                          top: 0,
                          left: 0
                        }}
                      />
                    </div>
                  </div>
                )}
              </div>
              <div className="col-12 col-sm-7 anim-4">
                <p className="description">{country.description[lang]}</p>
                <div className="btns-action anim-5">
                  <a className="btn btn-white" href={`/${country.id}/gallery`}>
                    {messages.viewGallery}
                  </a>
                </div>
              </div>
              <footer className="section-footer scrolldown">
                <a className="down">
                  <span className="icon" />
                  <span className="txt">{messages.next}</span>
                </a>
              </footer>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CountrySlide;