import React, { Component } from "react";

class Header extends Component {
  render() {
    const { countries, lang, messages, changeLang } = this.props;

    return (
      <header className="page-header navbar page-header-alpha scrolled-white menu-right topmenu-right">
        <ul className="languagepicker">
          <li className={lang === "es" ? "selected" : ""}>
            <a
              onClick={() => {
                changeLang("es");
              }}
            >
              <img alt="español" src="/img/spain.png" />
              ES
            </a>
          </li>
          <li className={lang === "en" ? "selected" : ""}>
            <a
              onClick={() => {
                changeLang("en");
              }}
            >
              <img alt="English" src="/img/united-states.png" />
              EN
            </a>
          </li>
        </ul>
        <button className="navbar-toggler site-menu-icon" id="navMenuIcon">
          <span className="menu-icon menu-icon-normal">
            <span className="bars">
              <span className="bar1" />
              <span className="bar2" />
              <span className="bar3" />
            </span>
          </span>
        </button>

        <a className="navbar-brand" href="/">
          <span className="logo">
            <img
              alt="teleSUR"
              className="light-logo"
              src="/img/telesur-logo.png"
            />
          </span>
          <span className="text hidden">
            <span className="line">{messages.title}</span>
            <span className="line sub">-</span>
          </span>
        </a>
        <div className="all-menu-wrapper" id="navbarMenu">
          <nav className="navbar-topmenu" />
          <nav className="navbar-mainmenu">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#home">
                  {messages.home}
                </a>
              </li>
              {countries.map(({ id, name }) => (
                <li className="nav-item" key={id}>
                  <a className="nav-link" href={`#${id}`}>
                    {name[lang]}
                    {window && window.location.hash === `#${id}` && (
                      <span className="sr-only">(current)</span>
                    )}
                  </a>
                </li>
              ))}
            </ul>
          </nav>
        </div>
      </header>
    );
  }
}

export default Header;
