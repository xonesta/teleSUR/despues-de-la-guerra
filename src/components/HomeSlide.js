import React, { Component } from "react";

class HomeSlide extends Component {
  render() {
    const { messages } = this.props;

    return (
      <div
        className="section section-home fullscreen-md fp-auto-height-responsive main-home section-centered"
        data-section="home"
      >
        <div className="section-wrapper fullwidth with-margin v-center">
          <div className="section-content anim">
            <div className="row">
              <div className="container-fluid">
                <div className="title-desc text-center">
                  <h1 className="display-4 display-title home-title anim-1 text-uppercase">
                    {messages.titleFirst}
                    <small style={{ padding: "0 14px" }}>
                      {messages.titleMiddle}
                    </small>
                    {messages.titleLast}
                  </h1>
                  <p className="anim-2">{messages.description}</p>
                </div>
                <div className="btns-action anim-3">
                  <a className="btn btn-outline-white" href="#afganistan">
                    {messages.next}
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="plus anim-4" style={{ top: "100px", left: "40px" }} />
          <div
            className="plus anim-4"
            style={{ top: "100px", right: "50px" }}
          />
          <div
            className="plus anim-4"
            style={{ bottom: "50px", left: "40px" }}
          />
          <div
            className="plus anim-4"
            style={{ bottom: "50px", right: "50px" }}
          />
          <div className="section-aside aside-bottom">
            <div className="text-right" />
          </div>
          <footer className="section-footer scrolldown">
            <a className="down">
              <span className="icon" />
              <span className="txt">{messages.next}</span>
            </a>
          </footer>
        </div>
      </div>
    );
  }
}

export default HomeSlide;
